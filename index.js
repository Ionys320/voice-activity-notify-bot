(async () => {

    const { mkdirSync, readFileSync, writeFileSync } = require('fs');

    const { token } = require('./config.json');

    const client = new (require('discord.js')).Client;

    const clientId = client.user.id;

    const lastEventDate = {};

    const memberAnnoucement = {};

    const subscriptionsDirectory = './subscriptions';

    const getSubscriptionsFile = guildId => `${subscriptionsDirectory}/${guildId}.txt`;

    const getSubscriptionsList = guildId => {

        try {

            return readFileSync(getSubscriptionsFile(guildId), 'utf8').split('\n').filter(Boolean)
        }

        catch(_){

            return [];

        }

    };

    const setSubscriptionsFile = (guildId, subscriptionsList) =>
        writeFileSync(getSubscriptionsFile(guildId), subscriptionsList.join('\n'), 'utf8');

    try { mkdirSync(subscriptionsDirectory) } catch(_){}

    client.on('message', async message => {

        const messageAuthorMember = message.author;

        if(messageAuthorMember.bot) return;

        const memberId = messageAuthorMember.id;

        const messageContent = message.content;

        if(messageContent && messageContent === (`<@!${clientId}>`)){

            const { guild } = message;

            const guildId = guild.id;

            const subscriptionsList = getSubscriptionsList(guildId);

            const memberIndex = subscriptionsList.indexOf(memberId);

            if(memberIndex !== -1){

                subscriptionsList.splice(memberIndex, 1);

                await message.channel.send('🔇');

            }

            else {

                subscriptionsList.push(memberId);

                await message.channel.send('🔊');

            }

            setSubscriptionsFile(guildId, subscriptionsList);

        }

        if(message.channel.type === 'dm'){

            memberAnnoucement[memberId] = message.cleanContent;

            await message.channel.send('✅');

        }

    });

    client.on('voiceStateUpdate', (oldMemberState, newMemberState) => {

        const joinedChannel = newMemberState.channel;

        const getJoinedChannelMemberCount = () => joinedChannel.members.size;

        const joinedMember = newMemberState.member;

        const joinedUser = joinedMember.user;

        if(joinedUser.bot

            || oldMemberState.channelID
            
            || !newMemberState.channelID
            
            || getJoinedChannelMemberCount() !== 1) return;

        const joinedMemberId = newMemberState.id;

        const joinedChannelGuild = joinedChannel.guild;

        const joinedChannelGuildId = joinedChannelGuild.id;

        const lastMemberEventDate = lastEventDate[joinedMemberId];

        const lastGuildEventDate = lastEventDate[joinedChannelGuildId];

        const currentDate = Date.now();

        const eventCooldown = 300000;

        if((lastMemberEventDate && currentDate - lastMemberEventDate < eventCooldown)

            || (lastGuildEventDate && currentDate - lastGuildEventDate < eventCooldown)) return;

        setTimeout(async () => {

            if(getJoinedChannelMemberCount() === 0) return;

            lastEventDate[joinedMemberId] = lastEventDate[joinedChannelGuildId] = currentDate;

            const subscriptionsList = getSubscriptionsList(joinedChannelGuildId);

            let usersRemoved = false;

            const joinedMemberAnnoucement = memberAnnoucement[joinedMemberId] ? `> \`${memberAnnoucement[joinedMemberId]}\`` : '';

            memberAnnoucement[joinedMemberId] = undefined;

            for(let subscriberIndex = 0; subscriberIndex < subscriptionsList.length; subscriberIndex++){

                const subscriberUserId = subscriptionsList[subscriberIndex];

                const subscriberUser = client.users.cache
                    .find(user => user.id === subscriberUserId);

                if(!subscriberUser){

                    subscriptionsList[subscriberIndex] = undefined;

                    if(!usersRemoved) usersRemoved = true;

                }

                else if(subscriberUserId !== joinedMemberId)

                    await subscriberUser.send(`<@${joinedMemberId}> \`[${joinedUser.tag}]\` 🎤 ${joinedChannelGuild.name} -> <#${joinedChannel.id}> | <https://techcord.me> 😜\n${joinedMemberAnnoucement}`);

            }

            if(usersRemoved)

                setSubscriptionsFile(joinedChannelGuildId, subscriptionsList.filter(Boolean));

            if(joinedMemberAnnoucement)

                await joinedMember.send('📨');

        }, 10000);

    });

    client.on('guildMemberUpdate', member => {

        if(member.id === clientId)

            member.setNickname(null);

    });

    await client.login(token);

})();